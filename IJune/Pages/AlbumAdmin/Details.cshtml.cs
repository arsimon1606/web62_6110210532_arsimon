using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IJune.Data;
using IJune.Models;

namespace IJune.Pages.AlbumAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly IJune.Data.IJuneContext _context;

        public DetailsModel(IJune.Data.IJuneContext context)
        {
            _context = context;
        }

        public Album Album { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Album = await _context.Albums.FirstOrDefaultAsync(m => m.AlbumID == id);

            if (Album == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
