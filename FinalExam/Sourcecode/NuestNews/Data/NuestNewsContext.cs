using NuestNews.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace NuestNews.Data
{
	public class NuestNewsContext : IdentityDbContext<NewsUser>
	{
		public DbSet<WorkSchedule> WorkSchedule {get; set;}
		
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseSqlite(@"Data source=WorkSchedule.db");
		}
	}
}