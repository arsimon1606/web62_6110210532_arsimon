using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using NuestNews.Data;
using NuestNews.Models;

namespace NuestNews.Pages.NuestAdmin
{
    public class CreateModel : PageModel
    {
        private readonly NuestNews.Data.NuestNewsContext _context;

        public CreateModel(NuestNews.Data.NuestNewsContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public WorkSchedule WorkSchedule { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.WorkSchedule.Add(WorkSchedule);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}