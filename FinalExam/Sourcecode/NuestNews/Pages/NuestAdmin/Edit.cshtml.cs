using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NuestNews.Data;
using NuestNews.Models;

namespace NuestNews.Pages.NuestAdmin
{
    public class EditModel : PageModel
    {
        private readonly NuestNews.Data.NuestNewsContext _context;

        public EditModel(NuestNews.Data.NuestNewsContext context)
        {
            _context = context;
        }

        [BindProperty]
        public WorkSchedule WorkSchedule { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            WorkSchedule = await _context.WorkSchedule.FirstOrDefaultAsync(m => m.WorkScheduleID == id);

            if (WorkSchedule == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(WorkSchedule).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WorkScheduleExists(WorkSchedule.WorkScheduleID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool WorkScheduleExists(int id)
        {
            return _context.WorkSchedule.Any(e => e.WorkScheduleID == id);
        }
    }
}
