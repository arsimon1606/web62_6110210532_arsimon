using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NuestNews.Data;
using NuestNews.Models;

namespace NuestNews.Pages.NuestAdmin
{
    public class IndexModel : PageModel
    {
        private readonly NuestNews.Data.NuestNewsContext _context;

        public IndexModel(NuestNews.Data.NuestNewsContext context)
        {
            _context = context;
        }

        public IList<WorkSchedule> WorkSchedule { get;set; }

        public async Task OnGetAsync()
        {
            WorkSchedule = await _context.WorkSchedule.ToListAsync();
        }
    }
}
