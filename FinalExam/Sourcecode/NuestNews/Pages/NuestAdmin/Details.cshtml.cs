using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using NuestNews.Data;
using NuestNews.Models;

namespace NuestNews.Pages.NuestAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly NuestNews.Data.NuestNewsContext _context;

        public DetailsModel(NuestNews.Data.NuestNewsContext context)
        {
            _context = context;
        }

        public WorkSchedule WorkSchedule { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            WorkSchedule = await _context.WorkSchedule.FirstOrDefaultAsync(m => m.WorkScheduleID == id);

            if (WorkSchedule == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
