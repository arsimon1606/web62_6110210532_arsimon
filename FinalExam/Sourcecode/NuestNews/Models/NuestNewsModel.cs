using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace NuestNews.Models
{
	public class WorkSchedule {
		public int WorkScheduleID {get; set;}
		
		public string Name {get; set;}
		public string Place {get; set;}
		
		[DataType(DataType.Date)]
		public string DateTime {get; set;}
		
		public string Time {get; set;} 
		public string Concept {get; set;}
		
		public string NewsUserId {get; set;}
		public NewsUser postUser {get; set;}

	}
	
	public class NewsUser : IdentityUser{
		public string FirstName {get; set;}
		public string LastName {get; set;}
	}
}