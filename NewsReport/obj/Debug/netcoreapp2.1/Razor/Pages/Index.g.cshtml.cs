#pragma checksum "E:\6110210532\NewsReport\Pages\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "f07f5e849630a8d84787b1f25a196f33273d5c2b"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(NewsReport.Pages.Pages_Index), @"mvc.1.0.razor-page", @"/Pages/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.RazorPages.Infrastructure.RazorPageAttribute(@"/Pages/Index.cshtml", typeof(NewsReport.Pages.Pages_Index), null)]
namespace NewsReport.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "E:\6110210532\NewsReport\Pages\_ViewImports.cshtml"
using NewsReport;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"f07f5e849630a8d84787b1f25a196f33273d5c2b", @"/Pages/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"88c01d45ccfb24904f468bbbfdaedb0a8afbac7d", @"/Pages/_ViewImports.cshtml")]
    public class Pages_Index : global::Microsoft.AspNetCore.Mvc.RazorPages.Page
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("style", new global::Microsoft.AspNetCore.Html.HtmlString("background-color :#F5DEB3"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 3 "E:\6110210532\NewsReport\Pages\Index.cshtml"
  
    ViewData["Title"] = "ระบบรายงานข่าว";

#line default
#line hidden
            BeginContext(76, 4, true);
            WriteLiteral("\t\r\n\t");
            EndContext();
            BeginContext(80, 47, false);
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("body", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "cdbfa7a60cba40218da4770289dc3298", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.BodyTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_BodyTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            EndContext();
            BeginContext(127, 166, true);
            WriteLiteral("\r\n\t\r\n\t\r\n\t\r\n\t<div class=\"jumbotron\" style=\"background-color :#FFA07A; color:#A52A2A\">\r\n\t\t<center><h2>ระบบรายงานข่าวโดยประชาชนเพื่อประชาชน</h2></center>\r\n\t</div>\r\n\t\r\n\r\n");
            EndContext();
#line 16 "E:\6110210532\NewsReport\Pages\Index.cshtml"
 foreach (var item in Model.NewsCategory) {

#line default
#line hidden
            BeginContext(338, 28, true);
            WriteLiteral("\t\t<div class=\"row\" >\r\n\t\t<h3>");
            EndContext();
            BeginContext(367, 38, false);
#line 18 "E:\6110210532\NewsReport\Pages\Index.cshtml"
       Write(Html.DisplayFor(model=>item.ShortName));

#line default
#line hidden
            EndContext();
            BeginContext(405, 13, true);
            WriteLiteral("</h3>\r\n\t\t<h4>");
            EndContext();
            BeginContext(419, 37, false);
#line 19 "E:\6110210532\NewsReport\Pages\Index.cshtml"
       Write(Html.DisplayFor(model=>item.FullName));

#line default
#line hidden
            EndContext();
            BeginContext(456, 17, true);
            WriteLiteral("</h4>\r\n\t\t</div>\r\n");
            EndContext();
            BeginContext(477, 190, true);
            WriteLiteral("\t\t<div class=\"row\" >\r\n\t\t\t<table class=\"table\" >\r\n\t\t\t<thead>\r\n\t\t\t<tr>\r\n\t\t\t\t<th>วันที่เกิดเหตุ</th>\r\n\t\t\t\t<th>เนื้อข่าว</th>\r\n\t\t\t\t<th>สถานะการยืนยัน </th>\r\n\t\t\t</tr>\r\n\t\t\t</thead>\r\n\t\t\r\n\t<tbody>\r\n");
            EndContext();
#line 33 "E:\6110210532\NewsReport\Pages\Index.cshtml"
     foreach (var newsItem in Model.News){
		if(newsItem.NewsCategoryID == item.NewsCategoryID){

#line default
#line hidden
            BeginContext(763, 42, true);
            WriteLiteral("\t\t<tr>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(806, 49, false);
#line 37 "E:\6110210532\NewsReport\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => newsItem.ReportDate));

#line default
#line hidden
            EndContext();
            BeginContext(855, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(911, 49, false);
#line 40 "E:\6110210532\NewsReport\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => newsItem.NewsDetail));

#line default
#line hidden
            EndContext();
            BeginContext(960, 55, true);
            WriteLiteral("\r\n            </td>\r\n            <td>\r\n                ");
            EndContext();
            BeginContext(1016, 49, false);
#line 43 "E:\6110210532\NewsReport\Pages\Index.cshtml"
           Write(Html.DisplayFor(modelItem => newsItem.NewsStatus));

#line default
#line hidden
            EndContext();
            BeginContext(1065, 30, true);
            WriteLiteral("\r\n            </td>\r\n\t\t</tr>\r\n");
            EndContext();
#line 46 "E:\6110210532\NewsReport\Pages\Index.cshtml"
		}
	}

#line default
#line hidden
            BeginContext(1104, 31, true);
            WriteLiteral("\t</tbody>\r\n\t</table>\r\n\t</div>\r\n");
            EndContext();
#line 51 "E:\6110210532\NewsReport\Pages\Index.cshtml"
}

#line default
#line hidden
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IndexModel> Html { get; private set; }
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel> ViewData => (global::Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary<IndexModel>)PageContext?.ViewData;
        public IndexModel Model => ViewData.Model;
    }
}
#pragma warning restore 1591
